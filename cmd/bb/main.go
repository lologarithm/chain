package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"

	"gitlab.com/lologarithm/chain"
)

func main() {
	httpAddr := os.Getenv("ADDR")
	if httpAddr == "" {
		httpAddr = ":8123"
	}
	log.Fatal(run(httpAddr))
}

func run(addr string) error {
	genesisBlock, _ := chain.GenerateBlock(chain.Block{}, 0) // chain.Block{0, t.String(), 0, "", ""}
	log.Printf("%#v", genesisBlock)
	var Blockchain = []chain.Block{genesisBlock}
	log.Println("Listening on ", addr)

	getblockchain := func(w http.ResponseWriter, r *http.Request) {
		bytes, err := json.MarshalIndent(Blockchain, "", "  ")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		io.WriteString(w, string(bytes))
	}
	writeBlock := func(w http.ResponseWriter, r *http.Request) {
		var m Message
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&m); err != nil {
			log.Printf("failed to decode: %s", err)
			respondWithJSON(w, r, http.StatusBadRequest, r.Body)
			return
		}
		defer r.Body.Close()
		newBlock, err := chain.GenerateBlock(Blockchain[len(Blockchain)-1], m.BPM)
		if err != nil {
			respondWithJSON(w, r, http.StatusInternalServerError, m)
			return
		}
		if chain.Validate(newBlock, Blockchain[len(Blockchain)-1]) {
			newBlockchain := append(Blockchain, newBlock)
			if len(newBlockchain) > len(Blockchain) {
				Blockchain = newBlockchain
			}
			log.Printf("BLOCKCHAIN:\n")
			for _, b := range Blockchain {
				log.Printf("%#v", b)
			}
			// spew.Dump(Blockchain)
		}
		respondWithJSON(w, r, http.StatusCreated, newBlock)
	}
	http.HandleFunc("/get", getblockchain)
	http.HandleFunc("/write", writeBlock)

	return http.ListenAndServe(addr, nil)
}

func respondWithJSON(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {
	response, err := json.MarshalIndent(payload, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("HTTP 500: Internal Server Error"))
		return
	}
	w.WriteHeader(code)
	w.Write(response)
}

type Message struct {
	BPM int
}
