package chain

import "time"

func GenerateBlock(oldBlock Block, BPM int) (Block, error) {
	var newBlock Block
	newBlock.Index = oldBlock.Index + 1
	newBlock.Timestamp = time.Now().String()
	newBlock.BPM = BPM
	newBlock.PrevHash = oldBlock.Hash
	newBlock.Hash = Hash(newBlock)
	return newBlock, nil
}
